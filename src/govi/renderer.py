from abc import ABC, abstractmethod
from enum import Enum

from govi.imagebuffer import ImageBuffer


class CoordinatesReferential(Enum):
    VIEWPORT = 0
    PICTURE = 1


class Overlay:
    def __init__(self, image_buffer: ImageBuffer):
        self.x = 0          # x position of the overlay
        self.y = 0          # y position of the overlay
        self.ov_width = 0   # width of the overlay
        self.ov_height = 0  # height of the overlay
        self.referential = CoordinatesReferential.PICTURE
        self.image_buffer = image_buffer


class VideoRenderer(ABC):
    def __init__(self):
        self.zoom_level = 1.
        self.current_zoom_level = 1.
        self.zoom_fit = False
        self.allocation_changed_flag = False
        self.overlay_id = 0
        self.overlays = {}

    @abstractmethod
    def get_renderer_widget(self):
        """
        :rtype gi.repository.Gtk.Widget
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def get_container_widget(self):
        return NotImplementedError()

    @abstractmethod
    def queue_draw(self):
        raise NotImplementedError()

    @abstractmethod
    def get_min_zoom(self):
        raise NotImplementedError()

    @abstractmethod
    def get_max_zoom(self):
        raise NotImplementedError()

    @abstractmethod
    def get_image_buffer_width(self):
        raise NotImplementedError()

    @abstractmethod
    def get_image_buffer_height(self):
        raise NotImplementedError()

    def draw_overlays(self):
        for overlay in self.overlays.values():
            self.do_draw_overlay(overlay)

    def do_draw_overlay(self, overlay):
        """ Draws the overlay on the rendering area by default does nothing.

        :param Overlay overlay:
        :return:
        """
        pass

    def add_overlay(self, overlay: Overlay):
        while self.overlay_id in self.overlays.keys():
            self.overlay_id += 1
        overlay_id = self.overlay_id
        self.overlay_id += 1
        self.overlays[overlay_id] = overlay
        return overlay_id

    def remove_overlay(self, overlay_id):
        del self.overlays[overlay_id]

    def get_zoom(self):
        return self.current_zoom_level

    def set_zoom(self, zoom_level):
        self.zoom_level = zoom_level
        self.current_zoom_level = min(max(self.get_min_zoom(), zoom_level), self.get_max_zoom())
        self.zoom_fit = False

    def set_zoom_fit(self, enable):
        self.zoom_fit = enable

    def get_zoom_to_fit(self, image_width, image_height):
        vp_width = self.get_container_widget().get_allocated_width()
        vp_height = self.get_container_widget().get_allocated_height()

        vp_aspect_ratio = vp_width / vp_height
        im_aspect_ratio = image_width / image_height

        if im_aspect_ratio > vp_aspect_ratio:
            return im_aspect_ratio / vp_aspect_ratio, vp_width / image_width
        else:
            return im_aspect_ratio / vp_aspect_ratio, vp_height / image_height

    @abstractmethod
    def get_pixel_for_x_y(self, viewport_x, viewport_y):
        """ Return the pixel of the image for the given pixel in viewport.

        :param viewport_x:
        :param viewport_y:
        :return:
        """
        raise NotImplementedError

    def set_image_buffer(self, image_buffer: ImageBuffer):
        """ Sets the image buffer to display and render it.

        :param ImageBuffer image_buffer:
        :return:
        """

    def set_visible_area_size(self, width, height):
        """ Sets the size of the visible area of the widget.

        :param int width:
        :param int height:
        :return:
        """
        pass

    def set_visible_area_position(self, x, y):
        """ Sets the position of the visible top corner of the widget

        :param int x:
        :param int y:
        :return:
        """
        pass