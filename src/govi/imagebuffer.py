from abc import ABC, abstractmethod
from enum import Enum

import numpy as np
from skimage import io as skio


class PixelFormat(Enum):
    RGBA8 = 0
    RGB8 = 1
    MONO8 = 2
    MONO12_LP = 3  # Left padded 12 bits data. For right padded, MONO16 will work fine.
    MONO16 = 4
    YCbCr420_8 = 5
    YCbCr422_8 = 6


class YPbPrVersion(Enum):
    BT601 = 0
    BT709 = 1
    BT2020 = 2


ypbpr_version_to_kb_kr = {
    YPbPrVersion.BT601: (0.114, 0.299),
    YPbPrVersion.BT709: (0.0722, 0.2126),
    YPbPrVersion.BT2020: (0.0593, 0.2627),
}


def ypbpr_to_rgb_matrix(version=YPbPrVersion.BT601):
    kb, kr = ypbpr_version_to_kb_kr[version]
    kg = 1 - kb - kr
    r = 2 - 2 * kr
    b = 2 - 2 * kb
    return 1., 0., r, 1., -kb / kg * b, -kr / kg * r, 1., b, 0.


class ImageBuffer(ABC):
    def __init__(self) -> None:
        super().__init__()
        self.format_version = 0

    @abstractmethod
    def get_width(self):
        """ width in pixel of the frame buffer

        :rtype: int
        :return:
        """
        raise NotImplementedError()

    @abstractmethod
    def get_height(self):
        """ height in pixels of the frame buffer

        :rtype: int
        """
        raise NotImplementedError()

    @abstractmethod
    def get_buffer(self):
        """ Buffer containing the pixels.

        :rtype: Iterable[int]
        """
        raise NotImplementedError()

    @abstractmethod
    def get_pixel_format(self):
        """ Format of the pixels

        :rtype: PixelFormat
        """
        raise NotImplementedError()

    def get_format_version(self):
        """ Version of the format"""
        return self.format_version

    @abstractmethod
    def get_buffer_as_bytes(self):
        """ Buffer translated as bytes

        :rtype: bytes
        """
        raise NotImplementedError()


class NumpyImageBuffer(ImageBuffer):
    def __init__(self, numpy_array, pixel_format=None, width=None, height=None):
        """

        :param numpy.ndarray numpy_array:
        """
        super().__init__()
        self.data = None
        self.width = width
        self.height = height
        self.format = pixel_format
        if self.format in (PixelFormat.YCbCr420_8, PixelFormat.YCbCr422_8):
            if self.width is None or self.height is None:
                raise ValueError('Width and Height must be provided.')
            self.format_version = YPbPrVersion.BT601
        self.set_numpy_array(numpy_array)

    def set_numpy_array(self, numpy_array):
        self.data = numpy_array
        shape = numpy_array.shape
        if self.width is None:
            self.width = shape[1]
            self.height = shape[0]
        if self.format is None:
            self.format = NumpyImageBuffer.guess_format(numpy_array)

    @staticmethod
    def guess_format(numpy_array):
        """"Guess the pixel format of a numpy array"""
        if numpy_array.dtype == np.uint8:
            depth = 8
        elif numpy_array.dtype == np.uint16:
            depth = 16
        else:
            raise TypeError('Unsupported type {}'.format(numpy_array.dtype))
        shape = numpy_array.shape
        dim = len(shape)
        if dim == 2:
            if depth == 8:
                pixel_format = PixelFormat.MONO8
            elif depth == 16:
                pixel_format = PixelFormat.MONO16
            else:
                raise TypeError('Unsuported type {} for 2D arrays'.format(numpy_array.dtype))
        elif dim == 3:
            n_channels = shape[2]
            if n_channels == 3 and depth == 8:
                pixel_format = PixelFormat.RGB8
            elif n_channels == 4 and depth == 8:
                pixel_format = PixelFormat.RGBA8
            else:
                raise TypeError('Unsupported type and channels combination ({}, {}).'.format(numpy_array.dtype, n_channels))
        else:
            raise TypeError('Only 2 and 3 dimensions images are supported')
        return pixel_format

    @staticmethod
    def new_from_path(path):
        return NumpyImageBuffer(skio.imread(path))

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def get_pixel_format(self):
        return self.format

    def get_buffer(self):
        return self.data

    def get_buffer_as_bytes(self):
        return self.data.tobytes()
