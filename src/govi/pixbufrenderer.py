import numpy as np
from gi.repository import Gtk, Gdk, GdkPixbuf

from govi.renderer import VideoRenderer
from govi.imagebuffer import PixelFormat, ImageBuffer, NumpyImageBuffer


class PixbufRenderer(VideoRenderer):
    def __init__(self):
        super().__init__()
        self.container = Gtk.Viewport()
        self.container.show()
        self.image = Gtk.Image()
        self.image.show()
        self.event_box = Gtk.EventBox()
        self.event_box.show()
        self.event_box.add(self.image)
        self.container.add(self.event_box)
        self._displayed_data = None
        self._int_pixbuffer = None
        self.image.connect('size-allocate', self.on_size_allocate)
        self.image.set_halign(Gtk.Align.START)
        self.image.set_valign(Gtk.Align.START)
        self.event_box.add_events(Gdk.EventMask.SCROLL_MASK | Gdk.EventMask.POINTER_MOTION_MASK)

    def get_renderer_widget(self):
        return self.event_box

    def get_container_widget(self):
        return self.container

    def get_pixel_for_x_y(self, viewport_x, viewport_y):
        return viewport_x / self.current_zoom_level, viewport_y / self.current_zoom_level

    def get_image_buffer_width(self):
        return self._int_pixbuffer.get_width()

    def get_image_buffer_height(self):
        return self._int_pixbuffer.get_height()

    def get_min_zoom(self):
        return 0.001

    def get_max_zoom(self):
        return 1.

    def set_zoom(self, zoom_level):
        super().set_zoom(zoom_level)
        self._update_picture()

    def set_zoom_fit(self, enable):
        super().set_zoom_fit(enable)
        self._update_zoom()
        self._update_picture()

    def set_image_buffer(self, image_buffer: ImageBuffer):
        super().set_image_buffer(image_buffer)
        if image_buffer.get_pixel_format() == PixelFormat.MONO8:
            mono_bytes = np.frombuffer(image_buffer.get_buffer_as_bytes(), dtype=np.uint8)
            new_buffer = bytearray(len(mono_bytes) * 3)
            array = np.frombuffer(new_buffer, dtype=np.uint8)
            array[::3] = mono_bytes[::]
            array[1::3] = array[::3]
            array[2::3] = array[::3]
            image_buffer = NumpyImageBuffer(array.reshape((image_buffer.get_height(), image_buffer.get_width(), 3)))
        elif image_buffer.get_pixel_format() != PixelFormat.RGB8:
            raise TypeError('Unsupported pixel format {}'.format(image_buffer.get_pixel_format()))
        displayed_data = image_buffer.get_buffer_as_bytes()
        self._int_pixbuffer = GdkPixbuf.Pixbuf.new_from_data(displayed_data, GdkPixbuf.Colorspace.RGB, False, 8,
                                                             image_buffer.get_width(), image_buffer.get_height(),
                                                             3 * image_buffer.get_width(),
                                                             None, None)
        self._displayed_data = displayed_data
        self._update_picture()

    def on_size_allocate(self, w, rectangle):
        if self.zoom_fit:
            self._update_zoom()
            self._update_picture()

    def _update_zoom(self):
        tx_width = self._int_pixbuffer.get_width()
        tx_height = self._int_pixbuffer.get_height()
        ar, zoom = self.get_zoom_to_fit(tx_width, tx_height)
        self.current_zoom_level = zoom

    def _update_picture(self):
        zoom_level = self.get_zoom()
        if zoom_level != 1:
            dest_width = int(self._int_pixbuffer.get_width() * zoom_level)
            dest_height = int(self._int_pixbuffer.get_height() * zoom_level)
            zoomed_pixbuf = self._int_pixbuffer.scale_simple(dest_width, dest_height, GdkPixbuf.InterpType.BILINEAR)
        else:
            zoomed_pixbuf = self._int_pixbuffer

        self.image.set_from_pixbuf(zoomed_pixbuf)

    def queue_draw(self):
        self._update_picture()
