# Copyright 2020, Matthieu Charbonnier

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
__description__ = "Opengl image renderer for GTK."
__license__ = "LGPLv3"
__uri__ = "https://gitlab.com/Charbonnier/gtk-opengl-video"
__version__ = "1.0.1"
__author__ = "Matthieu Charbonnier"
__email__ = "charboma38@gmail.com"

try:
    from .glrenderer import GlRenderer as Renderer
except ImportError:
    from .pixbufrenderer import PixbufRenderer as Renderer
