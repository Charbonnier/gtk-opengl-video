import ctypes
from abc import ABC, abstractmethod

import numpy as np
from OpenGL import GL as gl

from govi.imagebuffer import ImageBuffer, PixelFormat, ypbpr_to_rgb_matrix, YPbPrVersion

vertex_shader_code = [b"#version 330 core\n"
                      b"layout (location = 0) in vec3 aPos;\n"
                      b"layout (location = 1) in vec2 aTexCoord;\n"
                      b"\n"
                      b"out vec2 texCoord;\n"
                      b"\n"
                      b"void main()\n"
                      b"{\n"
                      b"    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
                      b"    texCoord = aTexCoord;\n"
                      b"}"]
rgb_shader_source = [b"#version 330 core\n"
                     b"out vec4 FragColor;\n"
                     b"\n"
                     b"in vec2 texCoord;\n"
                     b"\n"
                     b"uniform sampler2D videoTexture;\n"
                     b"\n"
                     b"void main()\n"
                     b"{\n"
                     b"    FragColor = texture(videoTexture, texCoord);\n"
                     b"}"]
mono_shader_source = [b"#version 330 core\n"
                      b"out vec4 FragColor;\n"
                      b"\n"
                      b"in vec2 texCoord;\n"
                      b"\n"
                      b"uniform sampler2D videoTexture;\n"
                      b"\n"
                      b"void main()\n"
                      b"{\n"
                      b"    FragColor = vec4(texture(videoTexture, texCoord).r);\n"
                      b"}"]
mono12_shader_source = [b"#version 330 core\n"
                        b"out vec4 FragColor;\n"
                        b"\n"
                        b"in vec2 texCoord;\n"
                        b"\n"
                        b"uniform sampler2D videoTexture;\n"
                        b"\n"
                        b"void main()\n"
                        b"{\n"
                        b"    float level;\n"
                        b"    level = texture(videoTexture, texCoord).r;\n"
                        b"    if (texCoord.x > 1. || texCoord.y > 1. || texCoord.x < 0. || texCoord.y < 0.)\n"
                        b"    {   FragColor = vec4(level);}\n"
                        b"    else\n"
                        b"    {   FragColor = vec4(level * 16);}\n"
                        b"}"]

ycbcr_conversion_code = (b"vec3 rgb;\n"
                         b"vec3 ypbpr; \n"
                         b"ypbpr.x = 255./219*(ycbcr.x - 16./255);\n"
                         b"ypbpr.y = 255./224*(ycbcr.y - 128./255);\n"
                         b"ypbpr.z = 255./224*(ycbcr.z - 128./255);\n"
                         b"rgb = ypbprMat * ypbpr;\n"
                         b"FragColor = vec4(rgb.x, rgb.y, rgb.z, 1.);\n"
                         )

ycbcr420_shader_source = [b"#version 330 core\n"
                          b"out vec4 FragColor;\n"
                          b"\n"
                          b"in vec2 texCoord;\n"
                          b"\n"
                          b"uniform mat3 ypbprMat;\n"
                          b"uniform sampler2D yTexture;\n"
                          b"uniform sampler2D cbTexture;\n"
                          b"uniform sampler2D crTexture;\n"
                          b"\n"
                          b"void main()\n"
                          b"{\n"
                          b"    vec3 ycbcr;\n"
                          b"    float var_1, var_2, var_3;\n"
                          b"    ycbcr.x = texture(yTexture, texCoord).r;\n"
                          b"    ycbcr.y = texture(cbTexture, texCoord).r;\n"
                          b"    ycbcr.z = texture(crTexture, texCoord).r;\n"
                          b"\n"
                          b"    if (texCoord.x > 1. || texCoord.y > 1. || texCoord.x < 0. || texCoord.y < 0.)\n"
                          b"    {\n"
                          b"        FragColor = vec4(ycbcr.x, ycbcr.y, ycbcr.z, 1.);\n"
                          b"    } else {\n"
                          + ycbcr_conversion_code
                          +
                          b"    }\n"
                          b"}"]

ycbcr422_shader_source = [b"#version 330 core\n"
                          b"out vec4 FragColor;\n"
                          b"\n"
                          b"in vec2 texCoord;\n"
                          b"\n"
                          b"uniform mat3 ypbprMat;\n"
                          b"uniform float texture_width; \n"
                          b"uniform sampler2D videoTexture;\n"
                          b"\n"
                          b"void main()\n"
                          b"{\n"
                          b"    vec3 ycbcr;\n"
                          b"    float var_1, var_2, var_3;\n"
                          b"    vec4 level = texture(videoTexture, texCoord);\n"
                          b"\n"
                          b"    if (texCoord.x > 1. || texCoord.y > 1. || texCoord.x < 0. || texCoord.y < 0.)\n"
                          b"    {\n"
                          b"        FragColor = level;\n"
                          b"    } else {\n"
                          b"        if (mod(texture_width * texCoord.x, 2.0) > 0.9) {\n"
                          b"            ycbcr.x = level.b;\n"
                          b"        } else {\n"
                          b"            ycbcr.x = level.r;\n"
                          b"        };\n"
                          b"        ycbcr.y = level.g;\n"
                          b"        ycbcr.z = level.a;\n"
                          + ycbcr_conversion_code
                          +
                          b"    }\n"
                          b"}"]


class GlObject:
    def __init__(self, vertice_array=None, element_array=None, use_pixel_buffer=False,
                 row_stride=5,
                 vertice_start=0, vertice_length=3,
                 texture_start=3, texture_length=2):
        self.vertex_array = gl.glGenVertexArrays(1)
        self.array_buffer = gl.glGenBuffers(1)
        self.element_buffer = gl.glGenBuffers(1)
        if use_pixel_buffer:
            self.pixel_buffer = gl.glGenBuffers(1)
        else:
            self.pixel_buffer = None
        self.row_stride = row_stride
        self.vertice_start = vertice_start
        self.vertice_length = vertice_length
        self.texture_start = texture_start
        self.texture_length = texture_length

        gl.glBindVertexArray(self.vertex_array)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.array_buffer)
        if vertice_array is not None:
            gl.glBufferData(gl.GL_ARRAY_BUFFER, vertice_array.nbytes, vertice_array, gl.GL_DYNAMIC_DRAW)
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.element_buffer)
        if element_array is not None:
            gl.glBufferData(gl.GL_ELEMENT_ARRAY_BUFFER, element_array.nbytes, element_array, gl.GL_STATIC_DRAW)
        gl.glVertexAttribPointer(0, self.vertice_length, gl.GL_FLOAT, gl.GL_FALSE, self.row_stride * 4,
                                 ctypes.c_void_p(0))
        gl.glEnableVertexAttribArray(0)

    def update_array_buffer(self, array_buffer):
        gl.glBindVertexArray(self.vertex_array)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.array_buffer)
        gl.glBufferData(gl.GL_ARRAY_BUFFER, array_buffer.nbytes, array_buffer, gl.GL_DYNAMIC_DRAW)


class BaseTexture(ABC):
    classes = {}
    ":type: dict[PixelFormat, Type(BaseTexture)]"
    binaries = {}
    ":type: dict[PixelFormat, bytes]"

    def __init__(self, gl_object: GlObject):
        self.gl_object = gl_object
        self.vertex_array = gl_object.vertex_array
        self.array_buffer = gl_object.array_buffer
        self.pixel_buffer = gl_object.pixel_buffer
        self.active = False
        self.program_id = None
        self.fragment_shader = None
        self.shader_program = None

    @abstractmethod
    def get_source(self):
        raise NotImplementedError()

    def compile_fragment_shader(self):
        if self.fragment_shader is not None:
            return self.fragment_shader
        self.fragment_shader = gl.glCreateShader(gl.GL_FRAGMENT_SHADER)
        gl.glShaderSource(self.fragment_shader, self.get_source())
        gl.glCompileShader(self.fragment_shader)
        success = gl.glGetShaderiv(self.fragment_shader, gl.GL_COMPILE_STATUS)
        if not success:
            msg = 'Fragment Shader Compilation Failed: {}'.format(gl.glGetShaderInfoLog(self.fragment_shader))
            raise RuntimeError(msg)
        return self.fragment_shader

    def get_bytes_buffer_from_image(self, image_buffer: ImageBuffer):
        buffer = image_buffer.get_buffer()
        if isinstance(buffer, bytearray):
            buffer = (gl.GLubyte * len(buffer)).from_buffer(buffer)
        return buffer

    def set_buffer(self, image_buffer: ImageBuffer):
        pass

    def set_program(self, program_id):
        self.program_id = program_id

    def dump_glsl_program_binaries(self):
        pass

    def set_from_binaries(self, binaries: bytes):
        pass

    def activate(self):
        pass


class ShaderProgramManager:
    def __init__(self):
        self.cached_programs = {}
        self.vertex_shader = None

    def build_vertex_shader(self):
        if self.vertex_shader is not None:
            return
        self.vertex_shader = gl.glCreateShader(gl.GL_VERTEX_SHADER)
        gl.glShaderSource(self.vertex_shader, vertex_shader_code)
        gl.glCompileShader(self.vertex_shader)
        success = gl.glGetShaderiv(self.vertex_shader, gl.GL_COMPILE_STATUS)
        if not success:
            msg = 'Vertex Shader Compilation Failed: {}'.format(gl.glGetShaderInfoLog(self.vertex_shader))
            raise RuntimeError(msg)

    def set_texture_program(self, texture: BaseTexture):
        try:
            program_id = self.cached_programs[texture.__class__]
        except KeyError:
            program_id = self.make_program(texture)
            self.cached_programs[texture.__class__] = program_id
        texture.set_program(program_id)

    def make_program(self, texture: BaseTexture):
        if self.vertex_shader is None:
            self.build_vertex_shader()
        shader_program = gl.glCreateProgram()
        fragment_shader = texture.compile_fragment_shader()
        gl.glAttachShader(shader_program, self.vertex_shader)
        gl.glAttachShader(shader_program, fragment_shader)
        gl.glLinkProgram(shader_program)
        success = gl.glGetProgramiv(shader_program, gl.GL_LINK_STATUS)
        if not success:
            msg = 'Shader program linking failed: {}'.format(gl.glGetProgramInfoLog(shader_program))
            raise RuntimeError(msg)
        gl.glDeleteShader(fragment_shader)
        return shader_program


class TextureManager:
    def __init__(self, gl_object: GlObject, shader_program_manager: ShaderProgramManager):
        self.cached_classes = {}
        ":type: dict[PixelFormat, BaseTexture]"
        self.gl_object = gl_object
        self.vertex_array = gl_object.vertex_array
        self.array_buffer = gl_object.array_buffer
        self.source_pbo = gl_object.pixel_buffer
        self.selected_texture = None
        ":type: BaseTexture"
        self.shader_program_manager = shader_program_manager

    def select_texture(self, pixel_format: PixelFormat):
        try:
            texture = self.cached_classes[pixel_format]
        except KeyError:
            texture = BaseTexture.classes[pixel_format](self.gl_object)
            self.shader_program_manager.set_texture_program(texture)
            self.cached_classes[pixel_format] = texture
        return texture

    def set_buffer(self, image_buffer: ImageBuffer):
        self.selected_texture = self.select_texture(image_buffer.get_pixel_format())
        self.selected_texture.set_buffer(image_buffer)

    def activate_texture(self):
        if self.selected_texture is not None:
            if self.selected_texture.program_id is None:
                self.shader_program_manager.set_texture_program(self.selected_texture)
            self.selected_texture.activate()
        return self.selected_texture


class Texture(BaseTexture):
    def __init__(self, gl_object, source, gl_img, gl_format, gl_type, border_color=None):
        super().__init__(gl_object)
        self.source = source
        self.texture_2D = None
        self.gl_img = gl_img
        self.gl_format = gl_format
        self.gl_type = gl_type
        self.unpack_alignment = 4

        if border_color is None:
            self.border_color = np.array([0.2, 0.2, 0.2, 1.], dtype=np.float32)
        else:
            self.border_color = border_color

    def build_texture(self):
        self.texture_2D = gl.glGenTextures(1)
        gl.glBindVertexArray(self.vertex_array)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.array_buffer)
        self.setup_texture(self.texture_2D)

        gl.glVertexAttribPointer(1, self.gl_object.texture_length, gl.GL_FLOAT, gl.GL_FALSE,
                                 self.gl_object.row_stride * 4, ctypes.c_void_p(self.gl_object.texture_start * 4))
        gl.glEnableVertexAttribArray(1)

    def setup_texture(self, texture):
        gl.glBindTexture(gl.GL_TEXTURE_2D, texture)
        gl.glTexParameterfv(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_BORDER_COLOR, self.border_color)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_BORDER)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_BORDER)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)

    def get_source(self):
        return self.source

    def activate(self):
        gl.glActiveTexture(gl.GL_TEXTURE0)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.texture_2D)
        gl.glUseProgram(self.program_id)
        gl.glUniform1i(gl.glGetUniformLocation(self.program_id, "videoTexture"), 0)

    def set_buffer(self, image_buffer: ImageBuffer):
        if self.texture_2D is None:
            self.build_texture()
        width = image_buffer.get_width()
        height = image_buffer.get_height()
        gl.glActiveTexture(gl.GL_TEXTURE0)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.texture_2D)

        buffer = self.get_bytes_buffer_from_image(image_buffer)

        if self.pixel_buffer is None:
            gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, width, height, 0, self.gl_format,
                            self.gl_type, buffer)
        else:
            gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, self.unpack_alignment)
            gl.glBindBuffer(gl.GL_PIXEL_UNPACK_BUFFER, self.pixel_buffer)
            gl.glBufferData(gl.GL_PIXEL_UNPACK_BUFFER, buffer, gl.GL_DYNAMIC_DRAW)
            gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, width, height, 0, self.gl_format,
                            self.gl_type, ctypes.c_void_p(0))
            gl.glBindBuffer(gl.GL_PIXEL_UNPACK_BUFFER, 0)


class RGB8Texture(Texture):
    def __init__(self, gl_object):
        super().__init__(gl_object, rgb_shader_source, gl.GL_RGB8, gl.GL_RGB, gl.GL_UNSIGNED_BYTE)


class RGBA8Texture(Texture):
    def __init__(self, gl_object):
        super().__init__(gl_object, rgb_shader_source, gl.GL_RGBA8, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE)


class Mono8Texture(Texture):
    def __init__(self, gl_object):
        super().__init__(gl_object, mono_shader_source, gl.GL_R8, gl.GL_RED, gl.GL_UNSIGNED_BYTE)
        self.unpack_alignment = 1

class Mono12Texture(Texture):
    def __init__(self, gl_object):
        super().__init__(gl_object, mono12_shader_source, gl.GL_R16, gl.GL_RED, gl.GL_UNSIGNED_SHORT)
        self.unpack_alignment = 2

class Mono16Texture(Texture):
    def __init__(self, gl_object):
        super().__init__(gl_object, mono_shader_source, gl.GL_R16, gl.GL_RED, gl.GL_UNSIGNED_SHORT)
        self.unpack_alignment = 2

class YCbCrTexture(Texture):
    def __init__(self, gl_object, source, gl_img, gl_format, gl_type, border_color=None):
        super().__init__(gl_object, source, gl_img, gl_format, gl_type, border_color)
        self.ypbpr_version = YPbPrVersion.BT601
        self.update_conversion_matrix = True

    def set_ypbpr_version(self, ypbpr_version: YPbPrVersion):
        if self.ypbpr_version == ypbpr_version:
            return
        self.ypbpr_version = ypbpr_version
        self.update_conversion_matrix = True

    def set_buffer(self, image_buffer: ImageBuffer):
        self.set_ypbpr_version(image_buffer.get_format_version())

    def do_conversion_matrix_update(self):
        if self.update_conversion_matrix:
            gl.glUniformMatrix3fv(gl.glGetUniformLocation(self.program_id, 'ypbprMat'), 1, True,
                                  ypbpr_to_rgb_matrix(self.ypbpr_version))
            self.update_conversion_matrix = False

    def activate(self):
        super().activate()
        self.do_conversion_matrix_update()


class YCbCr420_8_Texture(YCbCrTexture):
    """ For Y Cb Cr 420 pixels with 8 bits.
        420 packing: Y, Cb and Cr are packed separately in three consecutive arrays.
        Each pixel has one Y data and shares Cb and Cr with its four neighbours. Thus,
        Y length is height*width bytes, while Cb and Cr are height*width/4 bytes long.

        Since Y, Cb and Cr are split in 3 consecutive arrays, it has been chosen to use three independent monochrome
        texture to build the colored image.
    """
    def __init__(self, gl_object):
        super().__init__(gl_object, ycbcr420_shader_source, gl.GL_R8, gl.GL_RED, gl.GL_UNSIGNED_BYTE)
        self.y_texture = None
        self.cb_texture = None
        self.cr_texture = None

    def build_texture(self):
        self.y_texture = gl.glGenTextures(1)
        self.cb_texture = gl.glGenTextures(1)
        self.cr_texture = gl.glGenTextures(1)
        gl.glBindVertexArray(self.vertex_array)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.array_buffer)

        self.setup_texture(self.y_texture)
        self.setup_texture(self.cb_texture)
        self.setup_texture(self.cr_texture)

        gl.glVertexAttribPointer(1, self.gl_object.texture_length, gl.GL_FLOAT, gl.GL_FALSE,
                                 self.gl_object.row_stride * 4, ctypes.c_void_p(self.gl_object.texture_start * 4))
        gl.glEnableVertexAttribArray(1)

    def activate(self):
        gl.glActiveTexture(gl.GL_TEXTURE0)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.y_texture)
        gl.glActiveTexture(gl.GL_TEXTURE1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.cb_texture)
        gl.glActiveTexture(gl.GL_TEXTURE2)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.cr_texture)
        gl.glUseProgram(self.program_id)
        self.do_conversion_matrix_update()
        gl.glUniform1i(gl.glGetUniformLocation(self.program_id, "yTexture"), 0)
        gl.glUniform1i(gl.glGetUniformLocation(self.program_id, "cbTexture"), 1)
        gl.glUniform1i(gl.glGetUniformLocation(self.program_id, "crTexture"), 2)

    def set_buffer(self, image_buffer: ImageBuffer):
        super().set_buffer(image_buffer)
        if self.y_texture is None:
            self.build_texture()
        width = image_buffer.get_width()
        height = image_buffer.get_height()

        buffer = self.get_bytes_buffer_from_image(image_buffer)

        if self.pixel_buffer is None:
            raise NotImplementedError('YCbCr420 not supported without pixel buffer')
        width_2 = width // 2
        height_2 = height // 2
        gl.glBindBuffer(gl.GL_PIXEL_UNPACK_BUFFER, self.pixel_buffer)
        gl.glBufferData(gl.GL_PIXEL_UNPACK_BUFFER, buffer, gl.GL_DYNAMIC_DRAW)
        gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.y_texture)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, width, height, 0, self.gl_format,
                        self.gl_type, ctypes.c_void_p(0))

        gl.glBindTexture(gl.GL_TEXTURE_2D, self.cb_texture)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, width_2, height_2, 0, self.gl_format,
                        self.gl_type, ctypes.c_void_p(width * height))

        gl.glBindTexture(gl.GL_TEXTURE_2D, self.cr_texture)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, width_2, height_2, 0, self.gl_format,
                        self.gl_type, ctypes.c_void_p(width * height + width_2 * height_2))
        if self.pixel_buffer is not None:
            gl.glBindBuffer(gl.GL_PIXEL_UNPACK_BUFFER, 0)


class YCbCr422_8_Texture(YCbCrTexture):
    """ Use for 8 bits images encoded in Y Cb Cr 422.
        422 packing: Each pixel has a Y value and shares Cb and Cr with its neighbour on the same row.
        A row of the buffer stores alternatively Y and Cx value:
        Y Cb Y Cr Y Cb Y Cr Y Cb Y Cr
    """
    def __init__(self, gl_object, border_color=None):
        self.half_width = None

        # RGBA format is used, for the texture. Each set of RGBA bytes stores the data of 2 pixels. The shader will use:
        # - RGA as Y Cb Cr for the first pixel
        # - GBA as Cb Y Cr for the second pixel
        super().__init__(gl_object, ycbcr422_shader_source, gl.GL_RGBA8, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, border_color)

    def activate(self):
        super().activate()
        gl.glUniform1f(gl.glGetUniformLocation(self.program_id, "texture_width"), self.half_width * 2)

    def set_buffer(self, image_buffer: ImageBuffer):
        super().set_buffer(image_buffer)
        if self.texture_2D is None:
            self.build_texture()

        height = image_buffer.get_height()
        gl.glActiveTexture(gl.GL_TEXTURE0)
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.texture_2D)

        buffer = self.get_bytes_buffer_from_image(image_buffer)

        # With RGBA format OpenGL expects 4 bytes per pixel.
        # But the real format is actually 2 bytes per pixel.
        # For this reason OpenGL is set with half the number of pixels.
        self.half_width = image_buffer.get_width() // 2
        if self.pixel_buffer is None:
            gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, self.half_width, height, 0, self.gl_format,
                            self.gl_type, buffer)
        else:
            gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 2)
            gl.glBindBuffer(gl.GL_PIXEL_UNPACK_BUFFER, self.pixel_buffer)
            gl.glBufferData(gl.GL_PIXEL_UNPACK_BUFFER, buffer, gl.GL_DYNAMIC_DRAW)
            gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, self.gl_img, self.half_width, height, 0, self.gl_format,
                            self.gl_type, ctypes.c_void_p(0))
            gl.glBindBuffer(gl.GL_PIXEL_UNPACK_BUFFER, 0)


BaseTexture.classes = {PixelFormat.RGB8: RGB8Texture,
                       PixelFormat.RGBA8: RGBA8Texture,
                       PixelFormat.MONO8: Mono8Texture,
                       PixelFormat.MONO12_LP: Mono12Texture,
                       PixelFormat.MONO16: Mono16Texture,
                       PixelFormat.YCbCr420_8: YCbCr420_8_Texture,
                       PixelFormat.YCbCr422_8: YCbCr422_8_Texture}
