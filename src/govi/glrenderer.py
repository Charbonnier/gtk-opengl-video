from OpenGL import GL as gl
from gi.repository import Gtk, GObject, Gdk
import numpy as np

from govi.globject import GlObject, ShaderProgramManager, TextureManager
from govi.renderer import VideoRenderer, Overlay
from govi.imagebuffer import ImageBuffer


default_color = np.array([0.2, 1.0, 0.4, 1.], dtype=np.float32)
border_color = np.array([0.2, 0.2, 0.2, 1.], dtype=np.float32)

canvas_vertices = np.array([-1., -1., 0.,
                            -1.,  1., 0.,
                             1.,  1., 0.,
                             1., -1., 0.], dtype=np.float32)
VERTICES_N_COORDS = 3

cv_indices = np.array([0, 1, 2,   # First triangle
                       0, 2, 3],  # Second triangle
                      dtype=np.uint32)

n_canvas_vertices = len(cv_indices)

texture_coords = np.array([0., 1.,
                           0., 0.,
                           1., 0.,
                           1., 1.], dtype=np.float32)
TEXTURE_N_COORDS = 2
TOTAL_N_COORDS = VERTICES_N_COORDS + TEXTURE_N_COORDS

canvas_vertice_data = np.zeros((canvas_vertices.size + texture_coords.size,), dtype=np.float32)
canvas_vertice_data[0::TOTAL_N_COORDS] = canvas_vertices[0::VERTICES_N_COORDS]
canvas_vertice_data[1::TOTAL_N_COORDS] = canvas_vertices[1::VERTICES_N_COORDS]
canvas_vertice_data[2::TOTAL_N_COORDS] = canvas_vertices[2::VERTICES_N_COORDS]
canvas_vertice_data[3::TOTAL_N_COORDS] = texture_coords[0::TEXTURE_N_COORDS]
canvas_vertice_data[4::TOTAL_N_COORDS] = texture_coords[1::TEXTURE_N_COORDS]


class ScrolledGlArea(Gtk.Bin, Gtk.Scrollable):
    __gproperties__ = {
        'hadjustment': (Gtk.Adjustment, 'hadjustment', 'description', GObject.ParamFlags.READWRITE),
        'vadjustment': (Gtk.Adjustment, 'vadjustment', 'description', GObject.ParamFlags.READWRITE),
        'hscroll-policy': (Gtk.ScrollablePolicy, 'hscroll-policy', 'description', Gtk.ScrollablePolicy.NATURAL,
                           GObject.ParamFlags.READWRITE),
        'vscroll-policy': (Gtk.ScrollablePolicy, 'vscroll-policy', 'description', Gtk.ScrollablePolicy.NATURAL,
                           GObject.ParamFlags.READWRITE)
    }

    def __init__(self):
        self._hadjustment = None
        self._vadjustment = None
        self._hscroll_policy = None
        self._vscroll_policy = None
        super().__init__()

        self.gl_area = Gtk.GLArea()
        self.gl_area.show()
        self.gl_area.add_events(Gdk.EventMask.SCROLL_MASK | Gdk.EventMask.POINTER_MOTION_MASK)
        self.add(self.gl_area)

        self.canvas_width = 1
        self.canvas_height = 1
        self.canvas_fit_allocation = True

        self.gl_area.connect('scroll-event', self.on_scroll_event, None)

    def set_canvas_size(self, width, height):
        """

        :param width:
        :param height:
        :return:
        """
        self.canvas_width = width
        self.canvas_height = height
        self.update_adjustments()

    def set_canvas_fit_allocation(self, enable):
        """ If True, scrolling is deactivated and canvas size fits the ScrolledGlArea allocation.
        If False, the canvas size is given by self.canvas_width, self.canvas_height

        :param enable:
        :return:
        """
        self.canvas_fit_allocation = enable
        self.update_adjustments()

    def update_adjustments(self, allocation=None):
        if allocation is None:
            allocation = self.get_allocation()
        width = allocation.width
        height = allocation.height
        self.update_hadjustment(width)
        self.update_vadjustment(height)

    def update_hadjustment(self, width=None):
        if self._hadjustment is None:
            return
        if width is None:
            width = self.get_allocation().width
        if self.canvas_fit_allocation:
            self.texture_origin_left = 0
            self._hadjustment.configure(0, 0, 0, 10, 10, 0)
        else:
            upper = self.canvas_width - width
            if upper < self._hadjustment.get_value():
                self._hadjustment.configure(upper, 0, upper, 10, 10, 0)
            else:
                self._hadjustment.set_upper(upper)

    def update_vadjustment(self, height=None):
        if self._vadjustment is None:
            return
        if height is None:
            height = self.get_allocation().height
        if self.canvas_fit_allocation:
            self._vadjustment.configure(0, 0, 0, 10, 10, 0)
        else:
            upper = self.canvas_height - height
            if upper < self._vadjustment.get_value():
                self._vadjustment.configure(upper, 0, upper, 10, 10, 0)
            else:
                self._vadjustment.set_upper(upper)

    def get_top_corner(self):
        return self._hadjustment.get_value(), self._vadjustment.get_value()

    def show(self):
        super().show()
        self.gl_area.show()

    def do_set_property(self, prop, value):
        if prop.name == 'hadjustment':
            self._hadjustment = value
            if self._hadjustment is None:
                return
            self.update_hadjustment()
            self._hadjustment.connect('value-changed', self.on_hadjustment_value_changed, None)
        elif prop.name == 'vadjustment':
            self._vadjustment = value
            if self._vadjustment is None:
                return
            self.update_vadjustment()
            self._vadjustment.connect('value-changed', self.on_vadjustment_value_changed, None)
        elif prop.name == 'hscroll-policy':
            self._hscroll_policy = value
        elif prop.name == 'vscroll-policy':
            self._vscroll_policy = value
        else:
            raise KeyError(prop)

    def do_get_property(self, prop):
        if prop.name == 'hadjustment':
            return self._hadjustment
        elif prop.name == 'vadjustment':
            return self._vadjustment
        elif prop.name == 'hscroll-policy':
            return self._hscroll_policy
        elif prop.name == 'vscroll-policy':
            return self._vscroll_policy
        else:
            raise KeyError(prop)

    def do_size_allocate(self, rectangle):
        Gtk.Bin.do_size_allocate(self, rectangle)
        self.update_adjustments(rectangle)
        self.emit('resize_glarea')

    def scroll_rendering_area(self):
        self.emit('scrolled')

    def on_hadjustment_value_changed(self, hadjustment, data=None):
        self.scroll_rendering_area()

    def on_vadjustment_value_changed(self, vadjustment, data=None):
        self.scroll_rendering_area()

    def on_scroll_event(self, event_box, event, data=None):
        if event.direction == Gdk.ScrollDirection.LEFT:
            self._hadjustment.set_value(self._hadjustment.get_value() + self._hadjustment.get_step_increment())
        elif event.direction == Gdk.ScrollDirection.RIGHT:
            self._hadjustment.set_value(self._hadjustment.get_value() - self._hadjustment.get_step_increment())
        elif event.direction == Gdk.ScrollDirection.UP:
            self._vadjustment.set_value(self._vadjustment.get_value() + self._vadjustment.get_step_increment())
        elif event.direction == Gdk.ScrollDirection.DOWN:
            self._vadjustment.set_value(self._vadjustment.get_value() - self._vadjustment.get_step_increment())


GObject.type_register(ScrolledGlArea)
GObject.signal_new("scrolled", ScrolledGlArea, GObject.SignalFlags.RUN_LAST, GObject.TYPE_BOOLEAN, ())
GObject.signal_new("resize_glarea", ScrolledGlArea, GObject.SignalFlags.RUN_LAST, GObject.TYPE_BOOLEAN, ())


class GlRenderer(VideoRenderer):
    def __init__(self):
        super().__init__()
        self.scrolled_gl_area = ScrolledGlArea()
        self.scrolled_gl_area.show()
        self.gl_area = self.scrolled_gl_area.gl_area
        self._tex_width = 1
        self._tex_height = 1
        self._tex_origin_top = 0
        self._tex_origin_left = 0

        self._shader_program_manager = ShaderProgramManager()
        self._canvas_vertice_data = canvas_vertice_data.copy()
        self._canvas = None
        ":type: GlObject"
        self._canvas_tex = None
        ":type: TextureManager"

        self._overlay_z_coords = []
        self._overlay_z_coords_gen = None
        self._overlay_origin_x = None
        self._overlay_origin_y = None
        self._overlay_width_factor = 0
        self._overlay_height_factor = 0
        self._overlay_vertice_data = canvas_vertice_data.copy()
        self._overlay = None
        ":type: GlObject"
        self._overlay_tex = None
        ":type: TextureManager"

        self.need_aspect_ratio_update = True
        self.need_viewing_area_update = True
        self.gl_area.connect('realize', self.on_realize, None)
        self.gl_area.connect('render', self.on_render, None)
        self.scrolled_gl_area.connect('scrolled', self.on_scrolled, None)
        self.scrolled_gl_area.connect('resize_glarea', self.on_resize_glarea, None)
        self.zoom_fit = True
        self.gl_ready = False

    def get_renderer_widget(self):
        return self.scrolled_gl_area

    def get_container_widget(self):
        return self.scrolled_gl_area

    def get_max_zoom(self):
        return 10.

    def get_min_zoom(self):
        return 0.001

    def get_image_buffer_width(self):
        return self._tex_width

    def get_image_buffer_height(self):
        return self._tex_height

    def add_overlay(self, overlay: Overlay):
        overlay_id = super().add_overlay(overlay)
        n_overlays = len(self.overlays)
        self._overlay_z_coords = np.linspace(1, 0, n_overlays, dtype=np.float32)
        return overlay_id

    def draw_overlays(self):
        self._overlay_z_coords_gen = iter(self._overlay_z_coords)
        self._overlay_origin_x = self._tex_origin_left
        self._overlay_origin_y = - self._tex_origin_top
        vp_width, vp_height = self.gl_area.get_allocated_width(), self.gl_area.get_allocated_height()
        self._overlay_width_factor = 2 * self.current_zoom_level / vp_width
        self._overlay_height_factor = - 2 * self.current_zoom_level / vp_height
        gl.glBindVertexArray(self._overlay.vertex_array)
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        super().draw_overlays()
        gl.glDisable(gl.GL_BLEND)

    def do_draw_overlay(self, overlay):
        """

        :param Overlay overlay:
        :return:
        """
        overlay_buffer = overlay.image_buffer
        x = (overlay.x - self._overlay_origin_x) * self._overlay_width_factor - 1
        y = 1 + (overlay.y + self._overlay_origin_y) * self._overlay_height_factor
        w = overlay.ov_width * self._overlay_width_factor
        h = overlay.ov_height * self._overlay_height_factor
        z = next(self._overlay_z_coords_gen)
        self._overlay_vertice_data[0] = x
        self._overlay_vertice_data[1] = y
        self._overlay_vertice_data[5] = x
        self._overlay_vertice_data[6] = y + h
        self._overlay_vertice_data[10] = x + w
        self._overlay_vertice_data[11] = y + h
        self._overlay_vertice_data[15] = x + w
        self._overlay_vertice_data[16] = y
        self._overlay_vertice_data[2::TOTAL_N_COORDS] = z
        self._overlay.update_array_buffer(self._overlay_vertice_data)
        self._overlay_tex.set_buffer(overlay_buffer)
        self._overlay_tex.activate_texture()
        gl.glDrawElements(gl.GL_TRIANGLES, 6, gl.GL_UNSIGNED_INT, None)

    # noinspection PyUnusedLocal
    def on_realize(self, gl_area, data=None):
        gl_area.make_current()
        gl.glClearColor(*default_color)
        self._build_canvas()
        self._build_overlay_canvas()
        self.gl_ready = True

    # noinspection PyUnusedLocal
    def on_scrolled(self, scrolled_area, data=None):
        self.need_viewing_area_update = True

    # noinspection PyUnusedLocal
    def on_resize_glarea(self, scrolled_area, data=None):
        self.need_viewing_area_update = True
        self.need_aspect_ratio_update = True

    def set_zoom_fit(self, enable):
        super().set_zoom_fit(enable)
        self.scrolled_gl_area.set_canvas_fit_allocation(True)
        self.need_viewing_area_update = True
        self.need_aspect_ratio_update = True

    def set_zoom(self, level):
        super().set_zoom(level)
        self.scrolled_gl_area.set_canvas_size(self._tex_width * level, self._tex_height * level)
        self.scrolled_gl_area.set_canvas_fit_allocation(False)
        self.need_viewing_area_update = True
        self.need_aspect_ratio_update = True

    def set_image_buffer(self, picture: ImageBuffer):
        if not self.gl_ready:
            return
        self.gl_area.make_current()
        width = picture.get_width()
        height = picture.get_height()
        if width != self._tex_width and height != self._tex_height:
            self._tex_width = width
            self._tex_height = height
            self.need_aspect_ratio_update = True
            self.need_viewing_area_update = True
        self._canvas_tex.set_buffer(picture)
        self.gl_area.queue_render()

    def update_aspect_ratio(self):
        if not self.need_aspect_ratio_update:
            return
        self.need_aspect_ratio_update = False
        aspect_ratio, zoom = self.get_zoom_to_fit(self._tex_width, self._tex_height)
        self.current_zoom_level = zoom
        if aspect_ratio > 1:
            tc = np.array([0., aspect_ratio,
                           0., 0.,
                           1., 0.,
                           1., aspect_ratio], dtype=np.float32)
        else:
            tc = np.array([0., 1.,
                           0., 0.,
                           1. / aspect_ratio, 0.,
                           1. / aspect_ratio, 1.], dtype=np.float32)
        self.update_texture_position(tc)

    def update_viewing_area(self):
        if not self.need_viewing_area_update:
            return
        self.need_viewing_area_update = False
        vp_width, vp_height = self.gl_area.get_allocated_width(), self.gl_area.get_allocated_height()
        origin_x, origin_y = self.scrolled_gl_area.get_top_corner()
        # Texture coordinates
        zoomed_tex_width = self.current_zoom_level * self._tex_width
        zoomed_tex_height = self.current_zoom_level * self._tex_height
        left = origin_x / zoomed_tex_width
        top = origin_y / zoomed_tex_height
        bottom = top + vp_height / zoomed_tex_height
        right = left + vp_width / zoomed_tex_width
        tc = np.array([left, bottom,
                       left, top,
                       right, top,
                       right, bottom], dtype=np.float32)
        self._tex_origin_top = top * self._tex_height
        self._tex_origin_left = left * self._tex_width
        self.update_texture_position(tc)

    def update_texture_position(self, tc):
        self._canvas_vertice_data[VERTICES_N_COORDS::TOTAL_N_COORDS] = tc[0::TEXTURE_N_COORDS]
        self._canvas_vertice_data[VERTICES_N_COORDS + 1::TOTAL_N_COORDS] = tc[1::TEXTURE_N_COORDS]
        self._canvas.update_array_buffer(self._canvas_vertice_data)

    def _build_canvas(self):
        self._canvas = GlObject(self._canvas_vertice_data, cv_indices, use_pixel_buffer=True)
        self._canvas_tex = TextureManager(self._canvas, self._shader_program_manager)

    def _build_overlay_canvas(self):
        self._overlay = GlObject(element_array=cv_indices)
        self._overlay_tex = TextureManager(self._overlay, self._shader_program_manager)

    # noinspection PyUnusedLocal
    def on_render(self, gl_area, gl_context, data=None):
        if self.zoom_fit:
            self.update_aspect_ratio()
        else:
            self.update_viewing_area()

        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        gl.glBindVertexArray(self._canvas.vertex_array)
        if self._canvas_tex.activate_texture() is not None:
            gl.glDrawElements(gl.GL_TRIANGLES, 6, gl.GL_UNSIGNED_INT, None)
        self.draw_overlays()

    def get_pixel_for_x_y(self, x, y):
        texture_x = self._tex_origin_left + x / self.current_zoom_level
        texture_y = self._tex_origin_top + y / self.current_zoom_level
        return texture_x, texture_y

    def queue_draw(self):
        self.gl_area.queue_render()
