#
# Credits: Test picture from Myrthes Nunes Santos, https://www.flickr.com/photos/141373681@N03/41555311780
#


import numpy as np
from skimage import io as skio
from skimage.color import rgb2gray, rgb2ycbcr
from unittest import TestCase
from gi.repository import Gtk, GLib

from govi.glrenderer import GlRenderer
from govi.imagebuffer import NumpyImageBuffer, PixelFormat
from govi.pixbufrenderer import PixbufRenderer
from govi.renderer import Overlay


class ByteArrayImageBuffer(NumpyImageBuffer):
    def get_buffer(self):
        return bytearray(super().get_buffer())


class TestGLRenderer(TestCase):
    def __init__(self, methodName: str = ...) -> None:
        self.texture_index = 0
        self.stop = False
        image = skio.imread('./penguins.jpg')
        self.p_color = NumpyImageBuffer(image)
        mono8 = rgb2gray(image) * 255
        self.p_gray = NumpyImageBuffer(mono8.astype(np.uint8))
        self.p_gray_12 = NumpyImageBuffer((mono8 * 16).astype(np.uint16), pixel_format=PixelFormat.MONO12_LP)
        self.p_gray_16 = NumpyImageBuffer((mono8 * 256).astype(np.uint16), pixel_format=PixelFormat.MONO16)
        ycbcr = rgb2ycbcr(image)
        width = image.shape[1]
        height = image.shape[0]
        y_size = width*height
        c_size = y_size // 4

        p_ycbcr420 = np.zeros((height * width + height * width // 2), dtype=np.uint8)
        p_ycbcr420[0:y_size] = ycbcr[::, ::, 0].flatten()
        p_ycbcr420[y_size:y_size + c_size] = ycbcr[::2, ::2, 1].flatten()
        p_ycbcr420[y_size + c_size:] = ycbcr[::2, ::2, 2].flatten()

        p_ycbcr422 = np.zeros((height * width * 2), dtype=np.uint8)
        p_ycbcr422[0:y_size * 2:2] = ycbcr[::, ::, 0].flatten()
        p_ycbcr422[1:y_size * 2:4] = ycbcr[::, ::2, 1].flatten()
        p_ycbcr422[3:y_size * 2:4] = ycbcr[::, ::2, 2].flatten()

        self.p_ycbcr420 = NumpyImageBuffer(p_ycbcr420, pixel_format=PixelFormat.YCbCr420_8, height=height, width=width)
        self.p_ycbcr422 = NumpyImageBuffer(p_ycbcr422, pixel_format=PixelFormat.YCbCr422_8, height=height, width=width)
        self.p_bytearray = ByteArrayImageBuffer(image)

        super().__init__(methodName)

    def change_texture_timeout(self, textures, renderer, window, data=None):
        if self.stop:
            Gtk.main_quit()
            return False
        texture, name = textures[self.texture_index]
        renderer.set_image_buffer(texture)
        window.set_title('{} {}'.format(renderer.__class__.__name__, name))
        self.texture_index += 1
        if self.texture_index == len(textures):
            self.texture_index = 0
        return True

    @staticmethod
    def set_zoom_1(w, renderer):
        renderer.set_zoom(1)

    @staticmethod
    def set_zoom_fit(w, renderer):
        renderer.set_zoom_fit(True)

    @staticmethod
    def on_motion_event(gl_area, event, renderer):
        print(renderer.get_pixel_for_x_y(event.x, event.y))
    @staticmethod
    def change_buffer(picture):
        start = picture.width * 10
        end = start + picture.width * 30
        picture.bytes[start:end] = b'\xff' * picture.width * 30

    def stop_main(self):
        self.stop = True

    def test_glrenderer(self):
        renderer = GlRenderer()
        self.main(renderer, [(self.p_color, 'rgb'),
                             (self.p_gray, 'gray'),
                             (self.p_gray_12, 'mono12'),
                             (self.p_ycbcr420, 'YCbCr 420'),
                             (self.p_ycbcr422, 'YCbCr 422'),
                             (self.p_bytearray, 'bytearray')
                             ])

    def test_pixbufrenderer(self):
        renderer = PixbufRenderer()
        self.main(renderer, [(self.p_color, 'color'),
                             (self.p_gray, 'gray')
                             ])

    def main(self, renderer, textures_loop):
        ov_pix_width = 200
        ov_pix_height = 300
        overlay_array = np.ones((ov_pix_height, ov_pix_width, 4), np.uint8) * 100
        overlay_array[::, 0, ::] = 255
        overlay_array[::, ov_pix_width - 1, ::] = 255
        overlay_array[0, ::, ::] = 255
        overlay_array[ov_pix_height - 1, ::, ::] = 255
        overlay = Overlay(NumpyImageBuffer(overlay_array))
        overlay.x = 100
        overlay.y = 200
        overlay.ov_width = ov_pix_width
        overlay.ov_height = ov_pix_height

        renderer.get_renderer_widget().connect('motion-notify-event', TestGLRenderer.on_motion_event, renderer)
        renderer.add_overlay(overlay)
        scrl = Gtk.ScrolledWindow()
        scrl.show()
        scrl.add(renderer.get_container_widget())

        box = Gtk.Box()
        box.set_orientation(Gtk.Orientation.VERTICAL)
        box.pack_start(scrl, True, True, 0)
        box.show()

        button = Gtk.Button()
        button.set_label('Zoom 1')
        button.connect('clicked', TestGLRenderer.set_zoom_1, renderer)
        button.show()
        box.pack_start(button, False, False, 0)

        button = Gtk.Button()
        button.set_label('Zoom Fit')
        button.connect('clicked', TestGLRenderer.set_zoom_fit, renderer)
        button.show()
        box.pack_start(button, False, False, 0)

        window = Gtk.Window()
        window.set_title(renderer.__class__.__name__)
        window.show()
        window.add(box)
        window.connect('delete-event', lambda w, d: self.stop_main())
        self.stop = False
        GLib.timeout_add(1000, lambda: self.change_texture_timeout(textures_loop, renderer, window))
        Gtk.main()
